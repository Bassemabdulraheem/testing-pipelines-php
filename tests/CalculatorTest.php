<?php

use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase {
    // test the Calculator::add function
    public function testAdd () {
        $this->assertEquals(1, Calculator::add(0, 1));
    }

    // test the Calculator::subtract function
    public function testSubtract () {
        $this->assertEquals(1, Calculator::subtract(4, 3));
    }

    // test the Calculator::multiply function
    public function testMultiply () {
        $this->assertEquals(3, Calculator::multiply(3, 1));
    }

    // test the Calculator::divide function
    public function testDivide () {
        $this->assertEquals(4, Calculator::divide(16, 4));
    }

    // test the Calculator::squareRoot function

    /**
     * @dataProvider squareRootsDataProvider
     * @return void
     */
    public function testSquareRoot ($root, $number) {
        $this->assertEquals($root, Calculator::squareRoot($number));
    }

    public function squareRootsDataProvider()
    {
        return[
            [1, 1],
            [2, 4],
            [3, 9],
            [4, 16],
            [5, 25],
            [6, 36],
            [7, 49],
            [8, 64],
            [9, 81],
            [10, 100],
            [11, 121],
            [12, 144],
        ];
    }
}
